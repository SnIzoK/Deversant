class CreateMain < ActiveRecord::Migration
  def change
    create_table :mains do |t|
      t.text :main_description
      t.text :aboutus_first_description
      t.string :aboutus_first_image
      t.text :aboutus_second_description
      t.string :aboutus_second_image
      t.text :area_description
      t.string :area_first_image
      t.string :area_second_image
      t.string :area_third_image
      t.string :area_main_image
      t.string :first_number
      t.string :second_number
      t.string :third_number
      t.float :lattitude
      t.float :longtitude
      t.string :facebook
      t.string :youtube
      t.string :telegram
    end
  end
end
