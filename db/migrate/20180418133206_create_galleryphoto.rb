class CreateGalleryphoto < ActiveRecord::Migration
  def change
    create_table :galleryphotos do |t|
      t.string :image
      t.integer :gallery_id
    end
  end
end
