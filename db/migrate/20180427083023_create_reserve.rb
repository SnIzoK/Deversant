class CreateReserve < ActiveRecord::Migration
  def change
    create_table :reserves do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :comment
    end
  end
end
