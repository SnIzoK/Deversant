class CreateService < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :image
      t.string :title
      t.text :description
      t.boolean :show
    end
  end
end
