class CreateSlider < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string  :image
      t.boolean :show
    end
  end
end
