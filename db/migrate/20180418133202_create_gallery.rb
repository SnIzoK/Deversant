class CreateGallery < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :title
      t.date :date
    end
  end
end
