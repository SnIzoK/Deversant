class UserMailer < ApplicationMailer
  default from: ENV["smtp_gmail_user_name"]
  def reserve_email(reserve)
    @reserve = reserve
    mail(to: "viktor.o@voroninstudio.eu", subject: 'Заявка на консультацію прийнята')
  end
end
