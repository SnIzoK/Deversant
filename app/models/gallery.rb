class Gallery < ActiveRecord::Base
	mount_uploader :image, ImageUploader
  scope :show, -> {where(show: true)}
  has_many :galleryphotos
end
