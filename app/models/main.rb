class Main < ActiveRecord::Base
	mount_uploader :aboutus_first_image, ImageUploader
	mount_uploader :aboutus_second_image, ImageUploader
	mount_uploader :area_first_image, ImageUploader
	mount_uploader :area_second_image, ImageUploader
	mount_uploader :area_third_image, ImageUploader
	mount_uploader :area_main_image, ImageUploader
  scope :show, -> {where(show: true)}
end
