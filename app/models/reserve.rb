class Reserve < ActiveRecord::Base
  after_create :notify_admin
  def notify_admin
  		UserMailer.reserve_email(self).deliver_now
  end
end
