class Galleryphoto < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  belongs_to :galleryphotos
end
