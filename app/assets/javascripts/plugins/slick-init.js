$(".slider").slick({
  infinite: true,
  dots: false,
  arrows: false,
  autoplay: true,
});

$(".testemonials-slider").slick({
	arrows: true,
  	autoplay: false,
  	nextArrow: document.getElementById("NextArrow"),
  	prevArrow: document.getElementById("PrevArrow"),
});