RailsAdmin.config do |config|

  ### Popular gems integration

  # == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
  config.included_models = [Reserve,Galleryphoto,Gallery,Service,Review,Main,Slider]
  config.model Galleryphoto  do
    navigation_label "Портфоліо"
    label "Фотографії"
  end
  config.model Gallery do
    navigation_label "Портфоліо"
    label "Проекти"
    field :galleryphotos

    configure :date do
      strftime_format do
        "%d.%m.%Y"
      end
    end
    include_fields :title, :date
  end
  config.model Reserve do
    navigation_label "Звязок"
    label "Резервації"
    include_fields :name, :phone, :email, :comment
  end
  config.model Galleryphoto do
    field :image
  end
   config.model Main do
     navigation_label "Головна"
     label "Основна інформація"
     include_fields :main_description, :aboutus_first_description, :aboutus_first_image, :aboutus_second_description, :aboutus_second_image, :area_description, :area_first_image, :area_second_image, :area_third_image, :area_main_image,
     :first_number, :second_number, :third_number, :adress, :lattitude, :longtitude, :facebook, :youtube, :telegram
   end
   config.model Slider do
     navigation_label "Головна"
     label "Слайдер"
     include_fields :image, :show
   end
   config.model Review do
     navigation_label "Головна"
     label "Відгуки"
     include_fields :image,:name,:position,:text,:show
   end
   config.model Service do
     navigation_label "Головна"
     label "Послуги"
     include_fields :image,:title,:title_description,:description,:show
   end
end
